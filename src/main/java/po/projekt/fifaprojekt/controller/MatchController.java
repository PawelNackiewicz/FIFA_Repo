/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import po.projekt.fifaprojekt.dao.MatchDAO;
import po.projekt.fifaprojekt.model.Match;

/**
 *
 * @author Mateusz
 */
@Controller
@RequestMapping("/match")
public class MatchController {

    @Autowired
    private MatchDAO matchDAO;

    @RequestMapping("/showall")
    public ModelAndView shwoAvailable() {
        Iterable<Match> res = matchDAO.findAll();
        ModelAndView model = new ModelAndView("matchShow");
        model.addObject("matches", res);
        return model;
    }
    
    @RequestMapping("/show")
    public ModelAndView shwoAvailableUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetail = (UserDetails) auth.getPrincipal();
        Iterable<Match> res = matchDAO.listUserMathes(userDetail.getUsername());
        ModelAndView model = new ModelAndView("matchShow");
        model.addObject("matches", res);
        return model;
    }

    public ModelAndView enterResult() {
        ModelAndView model = new ModelAndView("enterResult");
        model.addObject("match", new Match());
        return model;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String setupForm(@RequestParam("id") Integer id, ModelMap model) {
        Match match = this.matchDAO.findOne(id);
        model.addAttribute("match", match);
        return "enterResult";
    }

    @RequestMapping("/editResult")
    public ModelAndView editResult() {
        Iterable<Match> show = matchDAO.findAll();
        ModelAndView model = new ModelAndView("editResult");
        model.addObject("matches", show);
        return model;
    }

    @RequestMapping(value = "/setResult", method = RequestMethod.POST)
    public String setReslt (Match m) {
        matchDAO.setResult(m.getTeam1Result(),m.getTeam2Result(),m.getId());
        return "redirect:/match/show?success";
    }
    
    @RequestMapping(value="/setResult", method = RequestMethod.GET)
    public String delete(@RequestParam("username") String username, @RequestParam("matchid") Integer matchid) {
        Match m = matchDAO.findOne(matchid);
        if(m.getPlayerIdHome().equals(username))
        {
            matchDAO.setResult(0, 3, matchid);
        }
        else if(m.getPlayerIdAway().equals(username))
        {
           matchDAO.setResult(3, 0, matchid);
        }
        else
            return "redirect:/match/show?error_cancel";
       
        return "redirect:/match/show?canceled";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@Valid Match match, BindingResult bindingResult) {
        System.out.println(match);
        if (bindingResult.hasErrors()) {
            return "/add?fail";
        } else {
            matchDAO.save(match);
            return "redirect:/match/add?success";
        }
    }

    @RequestMapping(value = "/add")
    public ModelAndView add() {
        ModelAndView model = new ModelAndView("enterResult");
        model.addObject("match", new Match());
        return model;
    }
}
