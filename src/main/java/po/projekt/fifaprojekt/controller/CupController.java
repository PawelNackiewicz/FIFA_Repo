/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import po.projekt.fifaprojekt.Service.CupServicesImpl;
import po.projekt.fifaprojekt.dao.CupDAO;
import po.projekt.fifaprojekt.dao.UserDAO;
import po.projekt.fifaprojekt.model.Cup;
import po.projekt.fifaprojekt.model.User;

/**
 *
 * @author Krzysztof
 */
@Controller
@RequestMapping("/cup")
public class CupController {

    @Autowired
    private CupDAO cupDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private CupServicesImpl cupServicesImpl;
    
    
    @RequestMapping(value="/add")
    public ModelAndView add() {
        ModelAndView model = new ModelAndView("addCup");
        model.addObject("cup", new Cup());    
        return model;
    }
    
    @RequestMapping(value="/edit", method = RequestMethod.GET)
    public String setupForm(@RequestParam("id") Integer id, ModelMap model) {
        Cup cup = this.cupDAO.findOne(id);
        model.addAttribute("cup", cup);
        return "addCup";
    }
    
    @RequestMapping(value="/delete", method = RequestMethod.GET)
    public String delete(@RequestParam("id") Integer id) {
        cupDAO.delete(id);
       
        return "redirect:/cup/showAdmin?deleted";
    }
    
   
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@Valid Cup cup, BindingResult bindingResult) {
        System.out.println(cup);
        if (bindingResult.hasErrors()) {
            return "/add?fail";
        } else {
            cup.setCanAdd(true);
            cup.setFreePlaces(cup.getCountMembers());
            System.out.println(cup);
            cupDAO.save(cup);
            return "redirect:/cup/add?success";
        }
    }
    @RequestMapping("/show")
    public ModelAndView shwoAvailable() {
        Iterable<Cup> res = cupDAO.findAll();
        ModelAndView model = new ModelAndView("showAvailable");
        model.addObject("cups", res);
        return model;
    }
    @RequestMapping("/showAdmin")
    public ModelAndView showAdmin() {
        Iterable<Cup> show = cupDAO.findAll();
        ModelAndView model = new ModelAndView("showAdmin");
        model.addObject("cups", show);
        return model;
    }
   
    @RequestMapping(value="/join", method = RequestMethod.GET)
    public String join(@RequestParam(value = "id") Integer cupid) {
        Cup joinToCup = cupDAO.findOne(cupid);
        int roundid=1;
        int freePlaces = joinToCup.getFreePlaces();
        System.out.println(freePlaces);
        
        //sprawdzenie czy zapelniony - wywolanie losowania TODO
        
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetail = (UserDetails) auth.getPrincipal();
        User u = userDAO.findByUsername(userDetail.getUsername());
        System.out.println(u);
        System.out.println(joinToCup);
       joinToCup.getUsers().add(u);
       System.out.println(joinToCup);
       try{
       cupDAO.save(joinToCup);
       }
       catch(org.springframework.dao.InvalidDataAccessApiUsageException e){
           System.out.println("juz zapisany do tego pucharu!");
           return "redirect:/cup/show?allreadysigned";
       }
       cupDAO.setFreePlaces(freePlaces-1,cupid);
       if(freePlaces-1<=0)
       {
           cupDAO.setCanAdd(false, cupid);
           cupServicesImpl.drawOfFirstRound(cupid,roundid);
       }
       
       //sort
       //for each 
        //a.drawOfFirstRound(id);
        //joinToCup.toString();
        return "redirect:/cup/show?success";
    }

}