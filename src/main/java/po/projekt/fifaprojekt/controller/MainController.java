package po.projekt.fifaprojekt.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import po.projekt.fifaprojekt.dao.CupDAO;
import po.projekt.fifaprojekt.dao.NewsDAO;
import po.projekt.fifaprojekt.model.Cup;
import po.projekt.fifaprojekt.model.News;

@Controller
public class MainController {
    
    @Autowired
    private CupDAO cupDAO;
    
    @Autowired
    private NewsDAO newsDAO;
    
    @RequestMapping("/")
    public ModelAndView home() {
        
        Iterable<Cup> res = cupDAO.findAvailableCups();
        ModelAndView model = new ModelAndView("home");
        Iterable<News> news = newsDAO.findAll();
        model.addObject("news", news);
        model.addObject("cups", res);
        return model;
    }
       
    
}
