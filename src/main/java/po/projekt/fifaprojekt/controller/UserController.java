/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import po.projekt.fifaprojekt.dao.UserDAO;
import po.projekt.fifaprojekt.model.User;

/**
 *
 * @author Krzysztof
 */
@Controller
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    private UserDAO userDAO;
    
    @RequestMapping(value= "/register", method = RequestMethod.GET)
    public ModelAndView registershow() {
        ModelAndView model = new ModelAndView("register");
        model.addObject("user", new User());    
        return model;
    }
    
    @RequestMapping(value= "/register", method = RequestMethod.POST)
     public String register(@Valid User user, BindingResult bindingResult) {
        System.out.println(user);
        if (bindingResult.hasErrors()) {
            return "/register?fail";
        } else {
            user.setRole("ROLE_USER");
            user.setEnabled(true); //tymczasowo zeby nowi userzy byli aktywni - pozniej jakas weryfikacja mailowa
            user.setDrawnId(99);
            userDAO.save(user);
            return "redirect:/user/register?success";
        }
    }
     
     @RequestMapping(value="/login")
    public ModelAndView add() {
        ModelAndView model = new ModelAndView("login-page");
            
        return model;
    }
}
