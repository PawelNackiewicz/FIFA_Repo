/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.dao;

import org.springframework.data.repository.CrudRepository;
import po.projekt.fifaprojekt.model.News;

/**
 *
 * @author Krzysztof
 */
public interface NewsDAO  extends CrudRepository<News, Long> {
    
}
