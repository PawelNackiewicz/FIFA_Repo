/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import po.projekt.fifaprojekt.model.Cup;
import po.projekt.fifaprojekt.model.UsersCups;

/**
 *
 * @author Pawel
 */
public interface UsersCupsDAO extends CrudRepository<UsersCups, Integer>{
    
    @Query("select u from UsersCups u where u.cupId = :id")
    public UsersCups findUsersById(@Param("id") Integer id);

}
