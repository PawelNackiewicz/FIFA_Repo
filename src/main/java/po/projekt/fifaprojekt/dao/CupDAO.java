/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import po.projekt.fifaprojekt.model.Cup;

/**
 *
 * @author Krzysztof
 */
public interface CupDAO extends CrudRepository<Cup, Integer> {

    @Query("select c from Cup c where c.canAdd = true order by c.freePlaces desc")
    public Iterable<Cup> findAvailableCups();

    @Query("select c from Cup c where c.id = :id")
    public Cup findById(@Param("id") Integer id);

    @Modifying
    @Transactional
    @Query("update Cup cup set cup.freePlaces = :freePlaces where cup.id = :id")
    void setFreePlaces(@Param("freePlaces") Integer freePlaces, @Param("id") Integer id);
    
    @Modifying
    @Transactional
    @Query("update Cup cup set cup.canAdd = :canAdd where cup.id = :id")
    void setCanAdd(@Param("canAdd") boolean canAdd, @Param("id") Integer id);
}
