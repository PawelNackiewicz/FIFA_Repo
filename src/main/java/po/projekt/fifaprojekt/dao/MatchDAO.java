/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import po.projekt.fifaprojekt.model.Match;

/**
 *
 * @author Mateusz
 */
public interface MatchDAO extends CrudRepository<Match, Integer> {

    @Query("select m from Match m")
    public Iterable<Match> listAllMathes();
    
    @Query("select m from Match m where m.playerIdHome=:username or m.playerIdAway=:username")
    public Iterable<Match> listUserMathes(@Param("username") String username);

    @Modifying
    @Transactional
    @Query("update Match match set match.team1Result = :team1Result, match.team2Result = :team2Result where match.id = :id")
    void setResult(@Param("team1Result") Integer team1Result, @Param("team2Result") Integer team2Result, @Param("id") Integer id);

}
 

        