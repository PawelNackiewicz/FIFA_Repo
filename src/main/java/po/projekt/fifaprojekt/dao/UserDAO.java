/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import po.projekt.fifaprojekt.model.User;

/**
 *
 * @author Krzysztof
 */
public interface UserDAO extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User findByUsername(String username);
    
    User findById(Integer id);
    
    @Modifying
    @Transactional
    @Query("update User user set user.drawnId = :drawnId where user.id = :id")
    void setDrawnId(@Param("drawnId") Integer drawnId, @Param("id") Integer id);
    
    

}
