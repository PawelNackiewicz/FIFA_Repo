/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Krzysztof
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
    private boolean enabled;
    @Email
    private String email;
    private String role;
    @Column(nullable = true)
    private Integer drawnId;

    @ManyToMany(mappedBy = "users")
    private Set<Cup> cups;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Cup> getCups() {
        return cups;
    }

    public void setCups(Set<Cup> cups) {
        this.cups = cups;
    }

    public int getDrawnId() {
        return drawnId;
    }

    public void setDrawnId(int drawnId) {
        this.drawnId = drawnId;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", password=" + password + ", enabled=" + enabled + ", email=" + email + ", role=" + role + ", drawnId=" + drawnId + '}';
    }

    public User() {
    }

    public User(Integer id, String username, Integer drawnId) {
        this.id = id;
        this.username = username;
        this.drawnId = drawnId;
    }
}
