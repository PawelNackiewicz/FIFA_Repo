package po.projekt.fifaprojekt.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
    
/**
 *
 * @author Pawel
 */
@Entity
@Table(name = "UsersCups")
public class UsersCups implements Serializable {

    public UsersCups() {
    }
    @Id
    private Integer id;
    private Integer userId;
    private Integer cupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCupId() {
        return cupId;
    }

    public void setCupId(Integer cupId) {
        this.cupId = cupId;
    }

}
