/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package po.projekt.fifaprojekt.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Mateusz
 */
@Entity
@Table(name = "matches")
public class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer cupId;
    private String cupName;
    private Date startDate;
    private String playerIdHome;
    private String playerIdAway;
    private Integer team1Result;
    private Integer team2Result;
    private Integer roundId;

    public Match() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCupId() {
        return cupId;
    }

    public void setCupId(Integer cupId) {
        this.cupId = cupId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getPlayerIdHome() {
        return playerIdHome;
    }

    public void setPlayerIdHome(String playerIdHome) {
        this.playerIdHome = playerIdHome;
    }

    public String getPlayerIdAway() {
        return playerIdAway;
    }

    public void setPlayerIdAway(String playerIdAway) {
        this.playerIdAway = playerIdAway;
    }

    public Integer getTeam1Result() {
        return team1Result;
    }

    public void setTeam1Result(Integer team1Result) {
        this.team1Result = team1Result;
    }

    public Integer getTeam2Result() {
        return team2Result;
    }

    public void setTeam2Result(Integer team2Result) {
        this.team2Result = team2Result;
    }

    public String getCupName() {
        return cupName;
    }

    public void setCupName(String cupName) {
        this.cupName = cupName;
    }

    public Integer getRoundId() {
        return roundId;
    }

    public void setRoundId(Integer roundId) {
        this.roundId = roundId;
    }

    @Override
    public String toString() {
        return "Match{" + "id=" + id + ", cupId=" + cupId + ", startDate=" + startDate + ", playerIdHome=" + playerIdHome + ", playerIdAway=" + playerIdAway + ", team1Result=" + team1Result + ", team2Result=" + team2Result + '}';
    }

}
