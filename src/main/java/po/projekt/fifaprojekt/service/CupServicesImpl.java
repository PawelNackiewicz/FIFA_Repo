package po.projekt.fifaprojekt.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import po.projekt.fifaprojekt.model.User;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.projekt.fifaprojekt.dao.CupDAO;
import po.projekt.fifaprojekt.dao.MatchDAO;
import po.projekt.fifaprojekt.dao.UserDAO;
import po.projekt.fifaprojekt.dao.UsersCupsDAO;
import po.projekt.fifaprojekt.model.Match;
import po.projekt.fifaprojekt.model.UsersCups;

/**
 *
 * @author Pawel
 */
@Service
public class CupServicesImpl implements CupServices {

    @Autowired
    private UsersCupsDAO usersCupDAO;

    @Autowired
    private UserDAO userDAO;
    
    @Autowired
    private MatchDAO matchDAO;
    
    
    @Autowired
    private CupDAO cupDAO;
    
    

    @Override
    public void drawOfFirstRound(Integer cupId, Integer roundId) {
        Iterable<UsersCups> users = usersCupDAO.findAll();
        List<Integer> userid = new ArrayList();
//        //petla
        for (UsersCups uc : users) {
            if (uc.getCupId().equals(cupId)) {
                userid.add(uc.getUserId());
            }
        }
        // users.add(usersCup.getUserId());
        List<User> usr = new ArrayList();           //tutaj lista zapisanych userow do pucharu
        for (Integer i : userid) {
            usr.add(userDAO.findById(i));
        }
        for (User user : usr) {
            Random generator = new Random();
            userDAO.setDrawnId(generator.nextInt(50), user.getId());
        }
        for (User user : usr) {
            System.out.println(user.toString());
        }

        Collections.sort(usr, new Comparator<User>() {
            @Override
            public int compare(User lhs, User rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.getDrawnId() > rhs.getDrawnId() ? -1 : (lhs.getDrawnId() < rhs.getDrawnId()) ? 1 : 0;
            }
        });

        for (User user : usr) {
            System.out.println(user.toString());
        }
        
        for(int i=0;i<usr.size();i+=2)
        {
            Match match = new Match();
            match.setCupId(cupId);
            match.setCupName(cupDAO.findById(cupId).getName());
            match.setPlayerIdHome(usr.get(i).getUsername());
            match.setPlayerIdAway(usr.get(i+1).getUsername());
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            java.sql.Date futureDate = CupServicesImpl.addDays(date, 3);
           // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            match.setStartDate(futureDate);
            match.setRoundId(roundId);
            matchDAO.save(match);
        }

        //wypisz Mecz o jakims id = 
//        player1 = zerowy element z listy obiektow
//        player2 ++ idnex
//        etc
    }

    public void setNextRound() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void drawOfFirstRound(UsersCups usersCups) {

    }
    public static java.sql.Date addDays(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        return new java.sql.Date(c.getTimeInMillis());
    }
}
