package po.projekt.fifaprojekt.Service;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import po.projekt.fifaprojekt.model.UsersCups;


/**
 *
 * @author Pawel
 */
@Service
public interface CupServices {
    public void drawOfFirstRound(UsersCups usersCups);
    //public void drawOfFirstRound();
    public void drawOfFirstRound(Integer cupId, Integer roundId);
    public void setNextRound();
}
