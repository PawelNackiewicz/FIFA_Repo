<%-- 
    Document   : matchShow
    Created on : 2018-03-19, 11:57:33
    Author     : Mateusz
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

        <title>Terminarz</title>
    </head>
    <body>
<%@ include file="navbar.jsp" %>
        <div class="jumbotron">
            <div class="container">
                <h1>Terminarz</h1>
                <p>Zobacz swoje najblizsze spotkania!</p>
                <c:if test="${not empty paramValues.error_canceled}">
                    <div class="alert alert-danger" style="margin-top: 20px;">
                        Nie udało się zrezygnować z meczu - skontaktuj się z Administratorem!
                    </div>
                </c:if>
                <c:if test="${not empty paramValues.canceled}">
                    <div class="alert alert-success" style="margin-top: 20px;">
                        Oddałeś spotkanie walkowerem pomyślnie!
                    </div>
                </c:if>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <table class="table table-striped">
                    <th>Puchar</th>
                    <th>Poczatek meczu</th>
                    <th>Gospodarz</th>
                    <th>Gość</th>
                    <th>Wynik</th>
                    <c:if test="${match.team1Result}!=null or ${match.team2Result}!=null}">
                    <th>Zrezygnuj</th>
                    </c:if>
                    <security:authorize ifAnyGranted="ROLE_ADMIN">
                    <th>Edytuj</th>
                    </security:authorize>
                        <c:forEach items="${matches}" var="match">
                        <tr>
                            <td>${match.cupName}</td>
                            <!--<td>Puchar Polski 2018 edytowany</td>-->
                            <td>${match.startDate}</td>
                            <td>${match.playerIdHome}</td>
                            <td>${match.playerIdAway}</td>
                            <!--<td>krzysztof</td>-->
                            <!--<td>pawi</td>-->
                            <td>${match.team1Result} - ${match.team2Result}</td>
                            <c:if test="${match.team1Result}!=null or ${match.team2Result}!=null}">
                            <td>
                                <security:authentication var="principal" property="principal" />
                                <a href="<%=request.getContextPath()%>/match/setResult?username=${principal.username}&matchid=${match.id}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>                                
                            </td>
                            </c:if>
                            <security:authorize ifAnyGranted="ROLE_ADMIN">
                             <td>
                                <a href="<%=request.getContextPath()%>/match/edit?id=${match.id}" class="btn btn-warning pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            </security:authorize>                            
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
</html>



