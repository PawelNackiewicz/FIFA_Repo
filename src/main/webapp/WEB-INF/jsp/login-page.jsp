<%-- 
    Document   : login-page
    Created on : 2018-04-23, 08:57:58
    Author     : Krzysztof
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <title>JSP Page</title>
    </head>
    
      <body class="text-center">
    <form class="form-signin" name='f' action='/FifaProjekt/j_spring_security_check' method='POST'>
      <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Zaloguj się</h1>
      <label for="userName" class="sr-only">Nazwa użytkownika</label>
      <input type="text" id="userName" class="form-control input-sm chat-input" placeholder="Nazwa użytkownika" name='j_username' required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="userPassword" class="form-control input-sm chat-input" placeholder="Hasło  " name='j_password' required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me" name='_spring_security_remember_me'> Zapamiętaj mnie
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Zalooguj</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
    </form>
  </body>
</html>
</html>
