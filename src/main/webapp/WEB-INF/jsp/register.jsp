<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
   
        
        
       
        <title>Rejestracja</title>
    </head>
    <body>
        <%@ include file="navbar.jsp" %>
                <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                
                
                <h1>Zarejestruj się</h1>
                <p>Tutaj możesz się zarejestrować</p>
                
                <c:if test="${not empty paramValues.success}">
                <div class="alert alert-success">
                    You've successfully registered to our awesome app!
                    <a href="/FifaProjekt/spring_security_login">LOGOWANIE</a>
                </div>
                </c:if>
                <c:if test="${not empty paramValues.fail}">
                <div class="alert alert-danger">
                    Wystąpił Bląd, spróbuj ponownie
                    
                </div>
                </c:if>
            
            </div>
        </div>

        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-lg-6">
                    <form:form commandName="user" action="register" method="POST" >
                        <div class="form-group">
                            <form:label path="username" for="username">Nazwa Użytkownika</form:label>
                            <form:input path="username" cssClass="form-control" id="username" placeholder="Podaj nazwę użytkownika" required="required"/><form:errors path="username" />
                        </div>
                        <div class="form-group">
                            <form:label path="password" for="password">Hasło Użytkownika</form:label>
                            <form:password path="password" cssClass="form-control" id="password" required="required"/><form:errors path="password" />
                        </div>
                        <div class="form-group">
                            <form:label path="email" for="email">Adres e-mail</form:label>
                            <form:input path="email" cssClass="form-control" id="email" placeholder="Podaj adres email" required="required"/><form:errors path="email" />
                        </div>
                        
                </div>
                <div class="col-lg-6">
                        
                        <form:button class="btn btn-success pull-right">Zarejestruj!</form:button>
                    </form:form>
                </div>
            </div>
        </div>
        
    </body>
</html>
