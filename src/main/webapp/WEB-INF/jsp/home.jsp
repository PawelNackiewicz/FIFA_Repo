<%-- 
    Document   : home
    Created on : 17-Mar-2018, 14:17:29
    Author     : Pawel
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="now" class="java.util.Date"/>  

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

        <title>JSP Page</title>
    </head>

    <body>

        <%@ include file="navbar.jsp" %>


        <div class="jumbotron">
            <div class="container">
                <h1>FIFA Aplikacja Turniejowa</h1>
                <p>Witaj na naszej aplikacji turniejowej do gry serii FIFA</p>
            </div>
        </div>

        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-md-8">
                    <h1>Aktualności ze świata FIFA</h1>
                    <c:forEach begin="0" end="2" items="${news}" var="news">
                        <table class="news-item">
                            <tr>
                                <td><h3>${news.title}</h3></td>
                            </tr>
                            <tr>
                                <td><p>${news.news}</p></td>
                            </tr>
                            <tr>
                                <td>Autor: ${news.publisher}, Opublikowano: ${news.published}</td>

                            </tr>
                            </tr>
                        </table>
                    </c:forEach>
                    <!--            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>-->
                </div>

                <div class="col-md-4">
                    <h2>Dostępne Puchary</h2>
                    <table class="table table-striped">
                        <th>Nazwa</th>
                        <th>Liczba wolnych miejsc</th>
                        <th>Zapisy od</th>
                        <th>Zapisz się</th>


                        <c:forEach begin="0" end="2" items="${cups}" var="cup">
                            <tr>
                                <td>${cup.name}</td>
                                <td>${cup.freePlaces} / ${cup.countMembers}</td>
                                <td>${cup.startDate}</td>
                                <td>
                                            <fmt:formatDate var="date_to_comare" value="${cup.startDate}" pattern="yyyy-MM-dd"/>
                                            <c:set var="today_date" value="<%=new java.util.Date()%>"/>
                                            <fmt:formatDate var="today_formated_date" value="${now}" pattern="yyyy-MM-dd"/>  
                                    
                                        <c:choose>
                                            
                                            <c:when test="${(cup.canAdd=='true') and (date_to_comare le today_formated_date)}">
                                                <security:authorize access="isAuthenticated()">
                                                <a href="<%=request.getContextPath()%>/cup/join?id=${cup.id}"><button class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button></a>
                                                </security:authorize> 
                                            </c:when>
                                                    <c:otherwise>

                                                <button class="btn btn-dark" disabled title="Zapisy nie rozpoczęły się!"><span class="glyphicon glyphicon-plus"></button>

                                            </c:otherwise>
                                        </c:choose>
                                    
                                </td>
                            </tr>
                            </tr>
                        </c:forEach>
                    </table>
                    <p><a class="btn btn-secondary" href="<%=request.getContextPath()%>/cup/show" role="button">Zobacz więcej &raquo;</a></p>
                </div>
            </div>

            <hr>

        </div> <!-- /container -->
    </body>
</html>
