<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

        <title>Dodawanie Pucharu</title>
    </head>
    <body>
        <%@ include file="navbar.jsp" %>
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
               
                <h1>Edytuj mecz</h1>
                <p>Wprowadz wynik spotkania</p>
                 <c:if test="${not empty paramValues.success}">
                    <div class="alert alert-success">
                        Sukces!
                    </div>
                </c:if>
            </div>
        </div>

        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-lg-6">
                    <form:form commandName="match" action="setResult" method="POST" >
                        <div class="form-group">
                            
                            <form:hidden path="id" cssClass="form-control" id="id" placeholder="Id Meczu" required="required"/><form:errors path="id" />
                            

                        </div>
                        <div class="form-group">
                            <form:label path="team1Result" for="team1Result">Gospodarze</form:label>
                            <form:input path="team1Result" cssClass="form-control" id="team1Result" placeholder="Ile bramek strzelili gospodarze?" required="required"/><form:errors path="team1Result" />
                        </div>
                        <div class="form-group">
                            <form:label path="team2Result" for="team1Result">Goscie</form:label>
                            <form:input path="team2Result" cssClass="form-control" id="team2Result" placeholder="Ile bramek strzelili goscie?" required="required"/><form:errors path="team2Result" />                       
                        </div>

                        <form:button class="btn btn-success pull-right">
                            Zapisz
                        </form:button>
                    </form:form>
                </div>
            </div>
        </div>

    </body>
</html>

