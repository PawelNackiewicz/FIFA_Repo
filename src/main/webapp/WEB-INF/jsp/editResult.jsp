<%-- 
    Document   : showadmin
    Created on : 2018-04-07, 19:42:57
    Author     : Krzysztof
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">

        <title>Edytuj Rezultat</title>
    </head>
    <body>
        <%@ include file="navbar.jsp" %>
        <div class="jumbotron">
            <div class="container">
               
                <h1>Lista spotkań</h1>
                <p>Tutaj możesz edytować wyniki meczów</p>
                 <c:if test="${not empty paramValues.deleted}">
                    <div class="alert alert-success">
                        Aktualizacja wyniku powieodła się!

                    </div>
                </c:if>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <table class="table table-striped">
                    <th>Puchar (ID)</th>
                    <th>Mecz (ID)</th>
                    <th>Poczatek meczu</th>
                    <th>Gospodarz</th>
                    <th>Gość</th>
                    <th>Wynik</th>
                        <c:forEach items="${matches}" var="match">
                        <tr>
                            <td>${match.cupId}</td>
                            <td>${match.id}</td>
                            <td>${match.startDate}</td>
                            <td>${match.playerIdHome}</td>
                            <td>${match.playerIdAway}</td>
                            <td>${match.team1Result} - ${match.team2Result}</td>
                            <td>
                                <a href="<%=request.getContextPath()%>/match/edit?id=${match.id}" class="btn btn-warning pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            <td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>



    </body>
</html>