<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

        <title>Dodawanie Pucharu</title>
    </head>
    <body>
        <%@ include file="navbar.jsp" %>
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                
                <h1>Nowy Puchar</h1>
                <p>Tutaj możesz dodać nowe Puchary</p>
                <c:if test="${not empty paramValues.success}">
                    <div class="alert alert-success">
                        Sukces!
                        <a href="<%=request.getContextPath()%>/cup/show"><button class="btn btn-success pull-right">Zobacz listę</button></a>
                    </div>
                </c:if>
            </div>
        </div>

        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-lg-6">
                    <form:form commandName="cup" action="save" method="POST" >
                        <div class="form-group">
                            <form:label path="name" for="cupName">Nazwa Pucharu</form:label>
                            <form:input path="name" cssClass="form-control" id="cupName" placeholder="Jak ma się nazywać Puchar?" required="required"/><form:errors path="name" />
                            <c:if test="${not empty paramValues.id}">
                                <form:hidden path="id" id="id"/>
                            </c:if>
                        </div>
                        <div class="form-group">
                            <form:label path="countMembers" for="countMembers" cssClass="radio-inline">Ile uczestników?</form:label><br />
                            <form:radiobutton path="countMembers" id="countMembers" value="16" label="16"/><form:errors path="countMembers" />
                            <form:radiobutton path="countMembers" id="countMembers" value="24" label="24"/><form:errors path="countMembers" />
                            <form:radiobutton path="countMembers" id="countMembers" value="32" label="32"/><form:errors path="countMembers" />
                            <form:radiobutton path="countMembers" id="countMembers" value="64" label="64"/><form:errors path="countMembers" />
                        </div>    
                        <div class="form-group">
                            <form:label path="startDate" for="startDate">Data rozpoczęcia pucharu</form:label>
                            <form:input path="startDate" cssClass="form-control" type="date" id="startDate" /><form:errors path="startDate" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <form:label path="endDate" for="endDate">Data zakońćzenia pucharu</form:label>
                            <form:input path="endDate" cssClass="form-control" type="date" id="endDate" /><form:errors path="endDate" />
                        </div>
                        <div class="form-group">
                            <form:label path="groupStage" for="groupStage">Wybierz typ turnieju</form:label><br />
                            <form:checkbox path="groupStage" id="groupStage" label="Faza grupowa" /><form:errors path="groupStage" />
                        </div>
                        <div class="col-lg-6">
                            <form:button class="btn btn-success pull-right">
                                <c:if test="${not empty paramValues.id}">
                                    Zaktualizuj
                                </c:if>
                                <c:if test="${empty paramValues.id}" >
                                    Zapisz
                                </c:if>
                            </form:button>
                        </form:form>
                    </div>
                </div>
            </div>

    </body>
</html>

