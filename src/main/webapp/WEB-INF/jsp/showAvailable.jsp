<%-- 
    Document   : showAvailable
    Created on : 17-Mar-2018, 14:18:24
    Author     : Pawel
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>    

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

        <title>Dołącz do pucharu</title>
        <script type="text/javascript" src="jquery-1.2.6.min.js"></script>
    </head>
    <body>
        <%@ include file="navbar.jsp" %>

        <div class="jumbotron">
            <div class="container">

                <h1>Dostępne puchary</h1>
                <security:authorize ifAnyGranted="ROLE_ADMIN"> <p>Tutaj możesz zobaczyć i edytować jak i usuwać Puchary</p></security:authorize>
                <security:authorize ifAnyGranted="ROLE_USER"><p>Tutaj możesz dołączyć do istniejącej rozgrywki</p></security:authorize>
                <c:if test="${not empty paramValues.allreadysigned}">
                    <div class="alert alert-danger" style="margin-top: 20px;">
                        Prawdopodobnie jesteś już zapisany w tym pucharze!
                    </div>
                </c:if>
                <c:if test="${not empty paramValues.success}">
                    <div class="alert alert-success" style="margin-top: 20px;">
                        Udało Ci się dołaczyć do pucharu!
                        <!--<a href="<%=request.getContextPath()%>/cup/show"><button class="btn btn-success pull-right">Zobacz listę</button></a>-->
                    </div>
                </c:if>

            </div>
        </div>

        <
        <div class="container">

            <input type="text" id="myInput" onkeyup="myFunctionToSortByName()" placeholder="Wyszukaj po nazwie" title="Type in a name">
            <input type="checkbox" id="canAddCB" value="showAvaliable" onclick="checkAvaliable()"> Pokaz dostepne<br>
            <br> <br>

            <div class="row">
                <table class="table table-striped" id="myTable">
                    <th>Nazwa</th>
                    <th>Liczba wolnych miejsc</th>
                    <th>Zapisy od</th>
                    <th>Dostępność</th>
                    <th>Zapisz się</th>
                        <security:authorize ifAnyGranted="ROLE_ADMIN">
                        <th>Zarządzaj</th>
                        </security:authorize>

                    <c:forEach items="${cups}" var="cup">
                        <tr>
                            <td>${cup.name}</td>
                            <td>${cup.freePlaces} / ${cup.countMembers}</td>
                            <td>${cup.startDate}</td>
                            <fmt:formatDate var="date_to_comare" value="${cup.startDate}" pattern="yyyy-MM-dd"/>
                            <c:set var="today_date" value="<%=new java.util.Date()%>"/>
                            <fmt:formatDate var="today_formated_date" value="${now}" pattern="yyyy-MM-dd"/>  
                            
                            <c:choose>
                                <c:when test="${(cup.canAdd=='true') and (date_to_comare le today_formated_date)}">
                                    <td class="canAdd">
                                        Możesz dołączyć 
                                        <br />
                                    </td>
                                    <td>
                                        <a href="<%=request.getContextPath()%>/cup/join?id=${cup.id}"><button class="btn btn-success">Dolacz do pucharu</button></a>
                                    </td>
                                </c:when>    
                                <c:otherwise>
                                    <td class="cantAdd">
                                        <c:choose>
                                        <c:when test="${date_to_comare gt today_formated_date}">
                                                Zapisy nie rozpoczęły się!
                                        </c:when>
                                        <c:otherwise>
                                            Brak miejsc!
                                        </c:otherwise>
                                        </c:choose>
                                                <br />
                                            </td>
                                            <td>
                                                <button class="btn btn-dark" disabled>Dolacz do pucharu</button>
                                            </td>
                                </c:otherwise>
                            </c:choose>
                            </td>
                            <security:authorize ifAnyGranted="ROLE_ADMIN">
                                <td>
                                    <a href="<%=request.getContextPath()%>/cup/edit?id=${cup.id}" class="btn btn-warning "><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="<%=request.getContextPath()%>/cup/delete?id=${cup.id}" class="btn btn-danger "><span class="glyphicon glyphicon-remove"></span></a>

                                </td>
                            </security:authorize>
                        </tr>
                    </c:forEach>
                </table>

                <script>
                    function myFunctionToSortByName() {
                        var input, filter, table, tr, td, i;
                        input = document.getElementById("myInput");
                        filter = input.value.toUpperCase();
                        table = document.getElementById("myTable");
                        tr = table.getElementsByTagName("tr");
                        for (i = 0; i < tr.length; i++) {
                            td = tr[i].getElementsByTagName("td")[0];
                            if (td) {
                                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                    tr[i].style.display = "";
                                } else {
                                    tr[i].style.display = "none";
                                }
                            }
                        }
                    }
                    function checkAvaliable() {

                        var checkBox = document.getElementById("canAddCB");

                        var elements = document.getElementsByClassName('cantAdd');
                        $('.cantAdd').each(function () {
                            if (checkBox.checked == true) {
                                $(this).parent().css('display', 'none');
                            } else {
                                $(this).parent().css('display', 'table-row');
                            }
                        });
                    }
                </script>
            </div>
        </div>



    </body>
</html>