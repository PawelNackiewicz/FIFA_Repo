<%-- 
    Document   : navbar
    Created on : 2018-04-07, 21:43:50
    Author     : Krzysztof
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <!--      <a class="navbar-brand" href="#">Fifa Puchar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>-->

    <div class="collapse navbar-collapse" id="navbarsExample05">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<%=request.getContextPath()%>/"><span class="glyphicon glyphicon-home">  Home <span class="sr-only">(current)</span></a>
            </li>
            <security:authorize access="hasAnyRole('ROLE_USER','ROLE_ADMIN')">
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/cup/show">Dostępne Puchary</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/match/show">Moje aktywne puchary</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/match/showall">Lista wszystkich spotkań</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-success navbar-right" href="<%=request.getContextPath()%>/cup/add">Dodaj Puchar</a>
                </li>
            </security:authorize>
            <!--		  <security:authorize ifAnyGranted="ROLE_ADMIN">
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administracja</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown05">
                          <a class="dropdown-item" href="<%=request.getContextPath()%>/cup/add">Dodaj Puchar</a>
                          <a class="dropdown-item" href="<%=request.getContextPath()%>/cup/showAdmin">Lista wszystkich Pucharów</a>
                          <a class="dropdown-item" href="<%=request.getContextPath()%>/match/editResult">Edytuj wyniki</a>
                        </div>
                      </li>
            </security:authorize>
            -->
            
        </ul>
        <security:authorize ifAnyGranted="ROLE_ANONYMOUS"> 
            <a href="<%=request.getContextPath()%>/user/login" class="btn btn-success navbar-right " style="margin-right: 8px;" ><span class="glyphicon glyphicon-log-in"></span>  Zaloguj</a>
            <a href="/FifaProjekt/user/register" class="btn btn-success navbar-right" ><span class="glyphicon glyphicon-registration-mark"></span> Rejestracja</a>						
        </security:authorize>    

        <security:authorize access="isAuthenticated()">
            <a href="<%=request.getContextPath()%>/j_spring_security_logout" class="navbar-right nav-link" > <span class="glyphicon glyphicon-log-in"></span> WYLOGUJ</a>
        </security:authorize>
    </div>
</nav>


