<%-- 
    Document   : showadmin
    Created on : 2018-04-07, 19:42:57
    Author     : Krzysztof
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">

        <title>Dołącz do pucharu</title>
    </head>
    <body>
<%@ include file="navbar.jsp" %>
        <div class="jumbotron">
            <div class="container">
                <h1>Lista pucharów</h1>
                <p>Tutaj możesz zobaczyć i edytować jak i usuwać Puchary</p>
                 <c:if test="${not empty paramValues.deleted}">
                    <div class="alert alert-success">
                        Usunięto pomyślnie!
                    </div>
                </c:if>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <table class="table table-striped">
                    <th>Nazwa</th>
                    <th>Liczba miejsc</th>
                    <th>Data rozpoczęcia</th>
                    <th>Data zakończenia</th>
                    <th>Zarządzaj</th>
                    
                            <c:forEach items="${cups}" var="cup">
                            <tr>
                                <td>${cup.name}</td>
                                <td>${cup.countMembers}</td>
                                <td>${cup.startDate}</td>
                                <td>${cup.endDate}</td>
                                <td>
                                    <a href="<%=request.getContextPath()%>/cup/edit?id=${cup.id}" class="btn btn-warning "><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a href="<%=request.getContextPath()%>/cup/delete?id=${cup.id}" class="btn btn-danger "><span class="glyphicon glyphicon-remove"></span></a>
                                    </td>
                                
                                </tr>
                        </c:forEach>
                </table>
            </div>
        </div>



    </body>
</html>